package com.example.demo.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.math.BigInteger;

import com.example.demo.model.PaymentDetail;

public class PaymentUtil {
	private static final String paymentKey = "gtKFFx";
	private static final String paymentSalt ="eCwWELxi";
	private static final String sUrl = "http://localhost:8080/payment/payment-response";
	private static final String fUrl = "http://localhost:8080/payment-response";
	public static PaymentDetail populatePaymentDetail(PaymentDetail paymentDetail) {
		String hashString="";
		Random rand = new Random();
		String randomId = Integer.toString(rand.nextInt()) + (System.currentTimeMillis()/1000L);
		String txnId = "Dev" + hashCal("SHA-256",randomId).substring(0,12);
		//String txnId = "Dev20f703cf0f11";
		paymentDetail.setTxnId(txnId);
		String hash="";
		String hashSequence = "key|txnid|amount|productInfo|firstname|email|||||||||||";
		hashString = hashSequence.concat(paymentSalt);
		hashString = hashString.replace("key", paymentKey);
		hashString = hashString.replace("txnid", txnId);
		hashString = hashString.replace("amount", paymentDetail.getAmount());
		hashString = hashString.replace("productInfo",paymentDetail.getProductInfo());
		hashString = hashString.replace("firstname", paymentDetail.getName());
		hashString = hashString.replace("email",paymentDetail.getEmail());
		//hash = hashCal("SHA-512",hashString);
		hash = getSHA_512(hashString);
		//String sha512 = org.apache.commons.codec.digest.DigestUtils.sha512Hex(hashString);
		System.out.println(hash);
		//System.out.println(sha512);
		paymentDetail.setHash(hash);
		paymentDetail.setfUrl(fUrl);
		paymentDetail.setsUrl(sUrl);
		paymentDetail.setKey(paymentKey);

		return paymentDetail;
	}
	public static String hashCal(String type,String str) {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algo = MessageDigest.getInstance(type);
			algo.reset();
			algo.update(hashseq);
			byte messageDigest[] = algo.digest();
			for(int i = 0;i<messageDigest.length;i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if(hex.length() ==1) {
					hexString.append("0");
				}
				hexString.append(hex);
			}
			
		}catch(NoSuchAlgorithmException nsae) {
			
		}
		return "82b3b0d59d1a6694cb1ef1dc42051fe9a62410191cbe454ee9cf957da176ba741012ac453902483e3475e63c48d7f2598ea35b1bb8112d85beaa3aa8339c221a";
		//return hexString.toString();
	}

	public static String getSHA512(String input){
        System.out.println(input);
		String toReturn = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.reset();
			digest.update(input.getBytes("utf8"));
			toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return toReturn;
	}

	public static String getSHA_512(String input)
	{
		try {
			System.out.println(input);
			// getInstance() method is called with algorithm SHA-512
			MessageDigest md = MessageDigest.getInstance("SHA-512");

			// digest() method is called
			// to calculate message digest of the input string
			// returned as array of byte
			byte[] messageDigest = md.digest(input.getBytes());

			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);

			// Convert message digest into hex value
			String hashtext = no.toString(16);

			// Add preceding 0s to make it 32 bit
			//while (hashtext.length() < 32) {
			//	hashtext = "0" + hashtext;
			//}

			// return the HashText
			return hashtext;
		}

		// For specifying wrong message digest algorithms
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

	}
}
